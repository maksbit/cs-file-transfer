﻿using CsFileTransfer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CsFileMapper
{
    public static class CsFileMapperCreator
    {
        public static bool passConstructorTypeNameParamsToBaseConstructor = false;

        public static void WriteCsMapperFiles(
            string targetDir,
            List<TypeDescription> types,
            Func<TypeDescription, List<string>> mapperInheritancesFunc,
            Dictionary<string, string> constructorTypeNameParams,
            List<string> namespaces,
            string mapperNamespace,
            bool isExtensionClass)
        {

            CsFileWriter.Write(targetDir, types,
                    getFileContentsFunc: type => BuildText(types, type, mapperInheritancesFunc, constructorTypeNameParams, namespaces, mapperNamespace, isExtensionClass),
                    getFileNameFunc: type => type.OldName + "Mapper");
        }

        private static string BuildText(
            List<TypeDescription> types,
            TypeDescription type,
            Func<TypeDescription, List<string>> mapperInheritancesFunc,
            Dictionary<string, string> constructorTypeNameParams,
            List<string> namespaces,
            string mapperNamespace,
            bool isExtensionClass)
        {
            Func<PropertyDescription, TypeDescription> getTypeByPropertyDescription = pd => types.First(x => x.Name == pd.PureType);

            var sb = new StringBuilder();

            var resultNamespaces = namespaces != null && namespaces.Any() ? namespaces : new List<string>() { "using System;" };

            sb.AppendNamespaces(resultNamespaces);

            sb.StartNamespaceBlock(string.IsNullOrWhiteSpace(mapperNamespace) ? "CsFileMapperCreator" : mapperNamespace);

            sb.StartTypeBlock("class", type.OldName + "Mapper", type, mapperInheritancesFunc, isStatic: isExtensionClass);

            //sb.AppendConstructor(type, constructorTypeNameParams, passConstructorTypeNameParamsToBaseConstructor);

            sb.AppendMapMethod(
                getTypeByPropertyDescription,
                type,
                td => "MapTo" + td.Name,
                td => td.OldName,
                td => td.Name,
                pd => pd.OldPureType,
                pd => pd.PureType,
                isExtensionClass);

            sb.EndNamespaceAndTypeBlocks();

            return sb.ToString();
        }

    }
}
