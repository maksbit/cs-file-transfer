﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsFileTransfer
{
    public class CsFileParserSettings
    {
        public string TypeNamePostfix = "Entity";

        public bool NeedTransferSummary = true;
        public bool NeedTransferAttributes = true;
        public bool NeedTransferNamespaces = true;
    }
}
