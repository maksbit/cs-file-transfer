﻿using CsFileTransfer.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CsFileTransfer
{
    public static class CsFileParser
    {
        public static CsFileParserSettings Settings = new CsFileParserSettings();

        public static List<TypeDescription> Parse(string folderPath)
        {
            var types = new List<TypeDescription>();

            var filePahts = Directory.GetFiles(folderPath, "*.cs", SearchOption.AllDirectories);

            //todo:exclude .cs files in obj/ and bin/ folders?
            foreach (var path in filePahts)
            {
                var lines = File.ReadAllLines(path, Encoding.UTF8);

                var typeItem = ParseAllLines(lines);
                //ugly hack: if AssemblyInfo.cs file parsed, it does not have class name => exclude items without class or enum names
                if (typeItem != null && !string.IsNullOrWhiteSpace(typeItem.Name))
                {
                    typeItem.RelativePath = path.Substring(folderPath.Length);
                    types.Add(typeItem);
                }
            }

            //change property names to new types
            var typeNameChangeMap = types.Where(x => x.OldName != x.Name).ToDictionary(x => x.OldName, x => x.Name);

            if (typeNameChangeMap.Any())
            {
                types.ForEach(x =>
                {
                    if (x.IsEnum)
                        return;

                    x.Properties.ForEach(p =>
                    {
                        if (typeNameChangeMap.ContainsKey(p.PureType))
                        {
                            var newName = typeNameChangeMap[p.PureType];

                            p.Type = p.Type.Replace(p.PureType, newName);
                            p.PureType = newName;
                        }
                    });

                });
            }

            return types;
        }

        private static bool TryGetSummary(string lineTrimmed, ref List<string> summary)
        {
            if (Settings.NeedTransferSummary && lineTrimmed.StartsWith("///"))
            {
                if (summary == null)
                    summary = new List<string>();

                summary.Add(lineTrimmed);

                return true;
            }

            return false;
        }

        private static bool TryGetAttributes(string lineTrimmed, ref List<string> attributes)
        {
            if (Settings.NeedTransferAttributes && lineTrimmed.StartsWith("["))
            {
                if (attributes == null)
                    attributes = new List<string>();

                attributes.Add(lineTrimmed);

                return true;
            }

            return false;
        }

        private static bool TryGetNamespaces(string lineTrimmed, ref List<string> namespaces)
        {
            if (Settings.NeedTransferNamespaces && lineTrimmed.StartsWith("using"))
            {
                if (namespaces == null)
                    namespaces = new List<string>();

                namespaces.Add(lineTrimmed);

                return true;
            }
            else if (Settings.NeedTransferNamespaces == false && namespaces == null)
            {
                namespaces = new List<string>();

                namespaces.Add("using System;");
            }

            return false;
        }

        private static TypeDescription ParseAllLines(string[] lines)
        {
            TypeDescription type = new TypeDescription();
            PropertyDescription currProp = null;
            EnumValueDescription currEnum = null;

            foreach (var line in lines)
            {
                if (string.IsNullOrWhiteSpace(type.Name))
                {
                    var lineTrimmed = line.TrimStart();
                    if (TryGetNamespaces(lineTrimmed, ref type.Namespaces))
                    {
                    }
                    else if (TryGetSummary(lineTrimmed, ref type.Summary))
                    {
                    }
                    else if (TryGetAttributes(lineTrimmed, ref type.Attributes))
                    {
                    }
                    else if (lineTrimmed.StartsWith("public class"))
                    {
                        type.OldName = lineTrimmed.Replace("public class", "").Trim();
                        type.Name = type.OldName + Settings.TypeNamePostfix;
                        type.Properties = new List<PropertyDescription>();

                        currProp = new PropertyDescription();
                    }
                    else if (lineTrimmed.StartsWith("public enum"))
                    {
                        type.OldName = lineTrimmed.Replace("public enum", "").Trim();
                        type.Name = type.OldName.Replace("Type", Settings.TypeNamePostfix + "Type");
                        type.IsEnum = true;
                        type.EnumValues = new List<EnumValueDescription>();

                        currEnum = new EnumValueDescription();
                    }
                }
                else
                {
                    if (type.IsEnum)
                    {
                        var lineTrimmed = line.TrimStart();
                        if (TryGetSummary(lineTrimmed, ref currEnum.Summary))
                        {
                        }
                        else if (Regex.IsMatch(lineTrimmed, "^[a-zA-Z]+"))
                        {
                            var lineNameValues = lineTrimmed.Split('=', StringSplitOptions.RemoveEmptyEntries);
                            if (lineNameValues.Length == 1)
                            {
                                currEnum.Name = lineNameValues[0].Trim().TrimEnd(',');
                            }
                            else
                            {
                                currEnum.Name = lineNameValues[0].Trim();
                                currEnum.Value = lineNameValues[1].Trim().TrimEnd(',');
                            }


                            type.EnumValues.Add(currEnum);

                            currEnum = new EnumValueDescription();
                        }
                    }
                    else
                    {

                        var lineTrimmed = line.TrimStart();
                        if (lineTrimmed.StartsWith("}"))//some method ends
                        {
                            currProp.Summary?.Clear();
                            currProp.Attributes?.Clear();
                        }
                        if (TryGetSummary(lineTrimmed, ref currProp.Summary))
                        {
                        }
                        else if (TryGetAttributes(lineTrimmed, ref currProp.Attributes))
                        {
                        }
                        else if (lineTrimmed.Contains("get;"))
                        {
                            var lineNameValues = lineTrimmed.Split(' ', StringSplitOptions.RemoveEmptyEntries);
                            currProp.Type = lineNameValues[1];
                            currProp.Name = lineNameValues[2];

                            currProp.IsArray = currProp.Type.EndsWith("[]");
                            currProp.IsNullable = currProp.Type.Contains("?");

                            currProp.PureType = currProp.IsNullable || currProp.IsArray
                                ? currProp.Type.Replace("[]", "").Replace("?", "")
                                : currProp.Type;
                            currProp.OldPureType = currProp.PureType;

                            type.Properties.Add(currProp);

                            currProp = new PropertyDescription();
                        }
                    }

                }
            }

            return type;
        }



    }
}
