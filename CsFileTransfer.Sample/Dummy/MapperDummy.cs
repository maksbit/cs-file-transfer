﻿using TestNamespace;

namespace CsFileTransfer.Sample
{
    public class MapperDummy
    {
        public TTarget Map<TSource, TTarget>(TSource obj)
        {
            if (typeof(TSource) == typeof(Template))
            {
                return (TTarget)(object)new TemplateMapper(this).Map((Template)(object)obj);
            }

            if (typeof(TSource) == typeof(TemplateType?))
            {
                return (TTarget)(object)new TemplateTypeMapper(this).Map((TemplateType?)(object)obj);
            }

            if (typeof(TSource) == typeof(TemplateType))
            {
                return (TTarget)(object)new TemplateTypeMapper(this).Map((TemplateType)(object)obj);
            }

            return default(TTarget);
        }
    }
}
