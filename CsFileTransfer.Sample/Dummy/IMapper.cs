﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsFileTransfer.Sample.Dummy
{
    public interface IMapper<TSource, TTarget>
    {
        TTarget Map(TSource source);
    }
}
