﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CsFileTransfer
{
    /// <summary>
    /// someroasfdjasdf
    /// </summary>
    [Obsolete]
    public class Template
    {
        /// <summary>
        /// Some summary
        /// next line summary
        /// </summary>
        [Required]
        [MaxLength(10)]
        public int[] SomeProperty { get; set; }

        /// <summary>
        /// method summary
        /// </summary>
        public void Do()
        {

        }

        private int value = 10;

        /// <summary>
        /// Some summary
        /// next line summary
        /// </summary>
        [Required]
        [MaxLength(10)]
        public decimal? OtherProperty { get; set; }

        /// <summary>
        /// Some summary
        /// next line summary
        /// </summary>
        [Required]
        [MaxLength(10)]
        public decimal?[] OtherPropertyArr { get; set; }

        public Template[] Contents { get; set; }

        public TemplateType TemplateType { get; set; }
        public TemplateType? TemplateTypeOpt { get; set; }
        public TemplateType?[] TemplateTypeArr { get; set; }

        public string SomeStr { get; set; }
        public int SomeInt { get; set; }
        public Int32 SomeInt32 { get; set; }
    }
}
