﻿using CsFileMapper;
using CsFileTransfer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using TestNamespace;

namespace CsFileTransfer.Sample
{

    partial class Program
    {
        private static CsFileParserSettings csFileParserSettings = new CsFileParserSettings()
        {
            TypeNamePostfix = "Entity",
            NeedTransferSummary = false,
            NeedTransferAttributes = false,
            NeedTransferNamespaces = false
        };

        //LOCAL
        //public static string copyCsFilesSourceDir = @"C:\D\Projects\VS\CSharp\Console\CsFileTransfer\CsFileTransfer.Sample\Source\";
        //public static string copyCsFilesTargetDir = @"C:\D\Projects\VS\CSharp\Console\CsFileTransfer\CsFileTransfer.Sample\Target\";

        //public static string createMappersSourceDir = @"C:\D\Projects\VS\CSharp\Console\CsFileTransfer\CsFileTransfer.Sample\Source\";
        //public static string createMappersTargetDir = @"C:\D\Projects\VS\CSharp\Console\CsFileTransfer\CsFileTransfer.Sample\Target\Mappers\";

        //WORK
        private static string copyCsFilesSourceDir = @"D:\Work\Work\SortedReact\Code\sorted.react.shipment.api\Sorted.React.Shipment.Api.DataContracts.Public\Inbound\";
        private static string copyCsFilesTargetDir = @"D:\Work\Work\SortedReact\Code\sorted.react.shipment.api\Sorted.React.Shipment.Api.Domain\";

        private static string createMappersSourceDir = @"D:\Work\Work\SortedReact\Code\sorted.react.shipment.api\Sorted.React.Shipment.Api.DataContracts.Public\Inbound\";
        private static string createMappersTargetDir = @"D:\Work\Work\SortedReact\Code\sorted.react.shipment.api\Sorted.React.Shipment.Api\Mappers\Inbound\";

        static void Main(string[] args)
        {
            //TestCopyArray();

            //CopyCsFiles();
            CreateMappers();

            //TestMapper();

            Console.ReadLine();
        }

        private static void TestMapper()
        {
            var tm = new TemplateMapper(new MapperDummy());
            var result = tm.Map(new Template()
            {
                TemplateTypeArr = new TemplateType?[] { TemplateType.value1, null },
                OtherPropertyArr = new decimal?[] { 10, null }
            });

            foreach (var item in result.TemplateTypeArr)
            {
                Log(item.ToString() ?? "null");
            }
        }

        private static void CreateMappers()
        {
            //parse
            CsFileParser.Settings = csFileParserSettings;
            var types = CsFileParser.Parse(createMappersSourceDir);

            //create mappers
            List<string> mapperInheritances = null; // new List<string>() {"object"};

            Dictionary<string, string> constructorTypeNameParams = null;// new Dictionary<string, string>() {{ "IGlobalMapper","mapper"} };

            var namespaces = new List<string>() { "using Sorted.React.Shipment.Api.Domain;", "using Sorted.React.Shipment.Api.DataContracts.Public.Inbound;" };

            var fileNamespace = "Sorted.React.Shipment.Api.Mappers.Inbound";

            //CsFileMapperCreator.passConstructorTypeNameParamsToBaseConstructor = true;

            Func<TypeDescription, List<string>> getInheritanceFunc = type =>
             {
                 //if (type.IsEnum)
                 //{
                 //    return new List<string>() { "MapperBase", "IMapper<${source}, ${target}>", "IMapper<${source}?, ${target}?>" };
                 //}

                 //return new List<string>() { "MapperBase", "IMapper<${source}, ${target}>" };

                 return null;

             };

            CsFileMapperCreator.WriteCsMapperFiles(createMappersTargetDir, types, getInheritanceFunc, constructorTypeNameParams, namespaces, fileNamespace, 
                isExtensionClass: true);

            Log("done create mappers");
        }

        private static void CopyCsFiles()
        {
            //parse
            CsFileParser.Settings = csFileParserSettings;

            var types = CsFileParser.Parse(copyCsFilesSourceDir);

            //copy cs files
            var fileNamespace = "TestNamespace";

            CsFileWriter.Write(copyCsFilesTargetDir, types, x => x.BuilidText(fileNamespace), x => x.Name);

            Log("done copy cs files");
        }

        private static void TestCopyArray()
        {
            var arr = new int?[] { 1, 2, 3, null };

            var newArr = ArrayCopierTest.CopyArray<int?, int?>(arr, TypesHelper.NetTypes.Contains("int"));

            arr[0] = 2;
            Log(arr[0].ToString());
            Log(newArr[0].ToString());
        }

        private static void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}
