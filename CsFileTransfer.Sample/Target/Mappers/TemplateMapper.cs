﻿using CsFileTransfer;
using CsFileTransfer.Sample;
using CsFileTransfer.Sample.Dummy;

namespace TestNamespace
{
    public class TemplateMapper: IMapper<Template,TemplateEntity>
    {
        private MapperDummy mapper = null;
                
        public TemplateMapper(MapperDummy mapper)
        {
            this.mapper = mapper;
            
        }

        public TemplateEntity Map(Template source)
        {
            var target = new TemplateEntity();
            target.SomeProperty = CopyArray(source.SomeProperty);
            target.OtherProperty = source.OtherProperty;
            target.OtherPropertyArr = CopyArray(source.OtherPropertyArr);
            target.Contents = CopyArrayUsingMapper<Template,TemplateEntity>(source.Contents);
            target.TemplateType = mapper.Map<TemplateType,TemplateEntityType>(source.TemplateType);
            target.TemplateTypeOpt = mapper.Map<TemplateType?,TemplateEntityType?>(source.TemplateTypeOpt);
            target.TemplateTypeArr = CopyArrayUsingMapper<TemplateType?,TemplateEntityType?>(source.TemplateTypeArr);
            target.SomeStr = source.SomeStr;
            target.SomeInt = source.SomeInt;
            target.SomeInt32 = source.SomeInt32;
            
            return target;
        }
        
        private TTarget[] CopyArrayUsingMapper<TSource, TTarget>(TSource[] arr)
        {
            if (arr == null)
                return null;

            var newArr = new TTarget[arr.Length];

            for (int i = 0; i < arr.Length; i++)
            {
                newArr[i] = mapper.Map<TSource, TTarget>(arr[i]);
            }

            return newArr;
        }

        private TSource[] CopyArray<TSource>(TSource[] arr)
        {
            if (arr == null)
                return null;

            var newArr = new TSource[arr.Length];

            for (int i = 0; i < arr.Length; i++)
            {
                newArr[i] = arr[i];
            }

            return newArr;
        }


    }
}