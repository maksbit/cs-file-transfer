﻿using CsFileTransfer;
using CsFileTransfer.Sample;
using CsFileTransfer.Sample.Dummy;

namespace TestNamespace
{
    public class TemplateTypeMapper: IMapper<TemplateType,TemplateEntityType>, IMapper<TemplateType?,TemplateEntityType?>
    {
        private MapperDummy mapper = null;
                
        public TemplateTypeMapper(MapperDummy mapper)
        {
            this.mapper = mapper;
            
        }

        public TemplateEntityType Map(TemplateType source)
        {
            return (TemplateEntityType)source;
        }

        public TemplateEntityType? Map(TemplateType? source)
        {
            return (TemplateEntityType?)source;
        }

    }
}