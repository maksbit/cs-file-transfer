﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TestNamespace
{
    /// <summary>
    /// someroasfdjasdf
    /// </summary>
    [Obsolete]
    public class TemplateEntity
    {
        
        /// <summary>
        /// Some summary
        /// next line summary
        /// </summary>
        [Required]
        [MaxLength(10)]
        public int[] SomeProperty{ get; set; }

        /// <summary>
        /// Some summary
        /// next line summary
        /// </summary>
        [Required]
        [MaxLength(10)]
        public decimal? OtherProperty{ get; set; }

        /// <summary>
        /// Some summary
        /// next line summary
        /// </summary>
        [Required]
        [MaxLength(10)]
        public decimal?[] OtherPropertyArr{ get; set; }

        public TemplateEntity[] Contents{ get; set; }

        public TemplateEntityType TemplateType{ get; set; }

        public TemplateEntityType? TemplateTypeOpt{ get; set; }

        public TemplateEntityType?[] TemplateTypeArr{ get; set; }

        public string SomeStr{ get; set; }

        public int SomeInt{ get; set; }

        public Int32 SomeInt32{ get; set; }

    }
}