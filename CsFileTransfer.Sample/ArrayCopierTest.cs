﻿using System;

namespace CsFileTransfer.Sample
{
    partial class Program
    {
        public static class ArrayCopierTest
        {
            private static int?[] CopyNetTypeArray(int[] arr)
            {
                if (arr == null)
                    return null;

                var newArr = new int?[arr.Length];
                arr.CopyTo(newArr, 0);

                return newArr;
            }

            private static MapperDummy mapper = new MapperDummy();
            public static TT[] CopyArray<TS, TT>(TS[] arr, bool isNetType)
            {
                if (arr == null)
                    return null;

                var newArr = new TT[arr.Length];

                for (int i = 0; i < arr.Length; i++)
                {
                    newArr[i] = isNetType ? (TT)(object)arr[i] : mapper.Map<TS, TT>(arr[i]);
                }

                return newArr;
            }
        }
    }
}
