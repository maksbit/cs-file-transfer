﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CsFileTransfer.Common
{
    public static class TypesHelper
    {
        public static string[] NetTypes = typeof(string).Assembly.GetTypes().Where(t => t.Namespace == "System").Select(x => x.Name)
              .Concat(new List<string> { "string", "bool",  "decimal", "float", "byte","sbyte","char","double","int","uint","long","ulong",
            "object","short","ushort"}).ToArray();
    }
}
