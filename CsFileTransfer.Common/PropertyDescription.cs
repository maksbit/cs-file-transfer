﻿using System.Collections.Generic;
using System.Text;

namespace CsFileTransfer.Common
{
    public class PropertyDescription
    {
        public string OldPureType { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public bool IsArray { get; set; }
        public bool IsNullable { get; set; }
        public string PureType { get; set; }

        public List<string> Summary;
        public List<string> Attributes;

        public void BuildText(StringBuilder sb)
        {
            sb.AppendSummary(Summary);

            sb.AppendAttributes(Attributes);

            sb.AppendProperty(Type, Name);
        }
    }
}