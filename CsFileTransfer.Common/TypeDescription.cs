﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CsFileTransfer.Common
{
    public class TypeDescription
    {
        public string OldName { get; set; }
        public string Name { get; set; }

        public List<PropertyDescription> Properties { get; set; }
        public List<EnumValueDescription> EnumValues { get; set; }

        public bool IsEnum { get; set; }
        public string RelativePath { get; set; }

        public List<string> Summary;
        public List<string> Attributes;
        public List<string> Namespaces;

        public string BuilidText(string newTypeNamespace)
        {
            if ((Properties == null || Properties.Any() == false) && (EnumValues == null || EnumValues.Any() == false))
                return null;

            var sb = new StringBuilder();

            sb.AppendNamespaces(Namespaces);

            sb.StartNamespaceBlock(newTypeNamespace ?? "CsFileTransfer");

            sb.AppendSummary(Summary, 1);

            sb.AppendAttributes(Attributes, 1);

            sb.StartTypeBlock(IsEnum ? "enum" : "class", Name, null, null);

            if (IsEnum)
            {
                EnumValues.ForEach(x => x.BuildText(sb));
            }
            else
            {
                Properties.ForEach(x => x.BuildText(sb));
            }

            sb.EndNamespaceAndTypeBlocks();

            return sb.ToString();
        }
    }
}
