﻿using CsFileTransfer.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CsFileTransfer.Common
{
    public static class CsFileWriter
    {
        public static void Write(string targetDir, List<TypeDescription> types, Func<TypeDescription, string> getFileContentsFunc,
            Func<TypeDescription, string> getFileNameFunc)
        {
            foreach (var type in types)
            {
                var contents = getFileContentsFunc(type);
                if (contents == null)
                    continue;

                var fileTargetPath = Path.Combine(targetDir, type.RelativePath);
                var fileTargetDir = Path.GetDirectoryName(fileTargetPath);
                Directory.CreateDirectory(fileTargetDir);

                fileTargetPath = Path.Combine(fileTargetDir, getFileNameFunc(type) + ".cs");
                
                File.WriteAllText(fileTargetPath, contents, Encoding.UTF8);
            }
        }
    }
}
