﻿using System.Collections.Generic;
using System.Text;

namespace CsFileTransfer.Common
{
    public class EnumValueDescription
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public List<string> Summary;

        public void BuildText(StringBuilder sb)
        {
            sb.AppendSummary(Summary);

            sb.AppendEnumValue(Name, Value);
        }
    }
}