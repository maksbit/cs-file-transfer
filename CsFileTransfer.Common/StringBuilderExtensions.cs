﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CsFileTransfer.Common
{
    public static class StringBuilderExtensions
    {
        public static void AppendNamespaces(this StringBuilder sb, List<string> namespaces)
        {
            if (namespaces != null && namespaces.Any())
            {
                namespaces.ForEach(x => sb.Append(x).AppendLine());
            }
        }

        public static void AppendSummary(this StringBuilder sb, List<string> summary, int indent = 1)
        {
            sb.AppendStringInternal(summary, newLine: true, indent: indent);
        }

        public static void StartTypeBlock(this StringBuilder sb, string typeType, string typeName,
            TypeDescription type = null,
            Func<TypeDescription, List<string>> mapperInheritancesFunc = null,
            bool isStatic = false)
        {
            sb.Append(@"
    public ");

            if (isStatic)
            {
                sb.Append("static ");
            }

            sb.Append(typeType)
            .Append(" ")
            .Append(typeName);

            sb.AppendInheritance(mapperInheritancesFunc, type);

            sb.Append(@"
    {
");

        }


        public static void AppendConstructor(this StringBuilder sb, TypeDescription type, Dictionary<string, string> constructorTypeNameParams,
            bool passConstructorTypeNameParamsToBaseConstructor)
        {
            if (constructorTypeNameParams == null && constructorTypeNameParams.Any() == false)
                return;

            //start private fields passed as params to constructor => private SomeType someField = null;
            if (passConstructorTypeNameParamsToBaseConstructor == false)
            {
                foreach (var keyValue in constructorTypeNameParams)
                {
                    sb.Append(@"
        private ")
                        .Append(keyValue.Key)
                        .Append(" ")
                        .Append(keyValue.Value)
                        .Append(@" = null;
        ");
                }
            }

            //start constructor
            sb.Append(@"        
        public ")
    .Append(type.OldName)
    .Append("Mapper(");

            //constructor params
            foreach (var keyValue in constructorTypeNameParams)
            {
                sb.Append(keyValue.Key)
                    .Append(" ")
                    .Append(keyValue.Value)
                    .Append(", ");
            }


            sb.Remove(sb.Length - 2, 2);

            sb.Append(@")");

            //base : (params...)
            if (passConstructorTypeNameParamsToBaseConstructor)
            {
                sb.Append(" : base(");
                foreach (var keyValue in constructorTypeNameParams)
                {
                    sb.Append(keyValue.Value)
                        .Append(", ");
                }

                if (constructorTypeNameParams != null && constructorTypeNameParams.Any())
                {
                    sb.Remove(sb.Length - 2, 2);
                }

                sb.Append(@")");
            }

            //constructor body => {   ...   }
            sb.Append(@"
        {
            ");


            //params to fields => this.someParam = someParam;
            if (passConstructorTypeNameParamsToBaseConstructor == false)
            {
                foreach (var keyValue in constructorTypeNameParams)
                {
                    sb.Append("this.")
                        .Append(keyValue.Value)
                        .Append(" = ")
                        .Append(keyValue.Value)
                        .Append(@";
            ");
                }
            }


            sb.Append(@"
        }
");


        }



        private static void AppendInheritance(this StringBuilder sb, Func<TypeDescription, List<string>> mapperInheritancesFunc, TypeDescription type)
        {
            if (type == null || mapperInheritancesFunc == null)
                return;

            var mapperInheritances = mapperInheritancesFunc(type);

            if (mapperInheritances?.Count > 0)
            {
                sb.Append(" : ");
                for (var i = 0; i < mapperInheritances.Count; i++)
                {
                    sb.Append(mapperInheritances[i].Replace("${source}", type.OldName).Replace("${target}", type.Name));
                    if (i != mapperInheritances.Count - 1)
                    {
                        sb.Append(", ");
                    }
                }
            }
        }

        public static void EndNamespaceAndTypeBlocks(this StringBuilder sb)
        {
            sb.Append(@"
    }
}");
        }

        public static void AppendProperty(this StringBuilder sb, string propertyType, string propertyName)
        {
            sb.Append(@"        public ")
                .Append(propertyType)
                .Append(" ")
                .Append(propertyName)
                .Append(" { get; set; }")
                .AppendLine();
        }

        public static void AppendEnumValue(this StringBuilder sb, string enumName, string enumValue)
        {
            sb.Append(@"        ").Append(enumName);

            if (!string.IsNullOrWhiteSpace(enumValue))
            {
                sb.Append(" = ")
                .Append(enumValue);
            }

            sb.Append(",")
            .AppendLine();
        }

        private static void AppendStringInternal(this StringBuilder sb, List<string> summary, bool newLine = false, int indent = 0)
        {
            if (summary != null && summary.Any())
            {
                summary.ForEach(x =>
                {
                    if (newLine)
                    {
                        sb.AppendLine();
                    }

                    if (indent > 0)
                    {
                        for (int i = 0; i < indent; i++)
                        {
                            sb.Append("    ");
                        }
                    }
                });
            }
        }

        public static void AppendAttributes(this StringBuilder sb, List<string> attributes, int indent = 1)
        {
            sb.AppendStringInternal(attributes, newLine: true, indent: indent);
        }

        public static void StartNamespaceBlock(this StringBuilder sb, string namespaceName)
        {
            sb.Append(@"
namespace ");
            sb.Append(namespaceName);
            sb.Append(@"
{");
        }

        public static void AppendMapMethod(this StringBuilder sb,
            Func<PropertyDescription, TypeDescription> getTypeByPropertyDescription,
            TypeDescription type,
            Func<TypeDescription, string> getMapFunctionNameFunc,
            Func<TypeDescription, string> getSourceTypeName,
            Func<TypeDescription, string> getTargetTypeName,
            Func<PropertyDescription, string> getSourcePropertyPureTypeName,
            Func<PropertyDescription, string> getTargetPropertyPureTypeName,
            bool isExtensionMethod = false
            )
        {
            if (type.IsEnum)
            {
                AppendEnumMapFunction(sb, type, getMapFunctionNameFunc, getSourceTypeName, getTargetTypeName, isExtensionMethod, isNullableCast: false);

                AppendEnumMapFunction(sb, type, getMapFunctionNameFunc, getSourceTypeName, getTargetTypeName, isExtensionMethod, isNullableCast: true);

                return;
            }


            sb.Append(@"
        public ");

            if (isExtensionMethod)
            {
                sb.Append("static ");
            }

            sb.Append(getTargetTypeName(type));

            sb.Append(" ");

            sb.Append(getMapFunctionNameFunc(type));

            sb.Append("(");

            if (isExtensionMethod)
            {
                sb.Append("this ");
            }

            //.Append(type.Name)
            //.Append(" Map(")
            sb.Append(getSourceTypeName(type))
         //.Append(type.OldName)
         .Append(@" source)
        {");

            sb.Append(@"
            if (source == null)
            {
                return null;
            }

            var target = new ")
            //.Append(type.Name)
                            .Append(getTargetTypeName(type))
                            .Append(@"();");

            foreach (var p in type.Properties)
            {
                sb.AppendLine().Append("            target.").Append(p.Name);

                var isCustomType = !TypesHelper.NetTypes.Contains(p.PureType);

                if (p.IsArray)
                {
                    if (isCustomType)
                    {
                        //sb.AppendGenericFuncWithOneParameterCall(" = CopyArrayUsingMapper",
                        //    getSourcePropertyPureTypeName(p) + (p.IsNullable ? "?" : ""),
                        //    getTargetPropertyPureTypeName(p) + (p.IsNullable ? "?" : ""),
                        //    "source." + p.Name);
                        // sb.Append(";");
                        var typeDesc = getTypeByPropertyDescription(p);

                        sb.Append(" = source." + p.Name + "?.Select(x => x." + getMapFunctionNameFunc(typeDesc));

                        sb.Append("()).ToArray();");
                    }
                    else
                    {
                        //sb.Append(" = CopyArray(source.").Append(p.Name).Append(@");");
                        sb.Append(" = source.").Append(p.Name).Append(@".CopyArray();");
                    }
                }
                else
                {
                    if (isCustomType)
                    {
                        var typeDesc = getTypeByPropertyDescription(p);

                        // = source.Prop1.MyMapperFunc();
                        sb.Append(" = source." + p.Name + "." + getMapFunctionNameFunc(typeDesc) + "();");

                        // = mapper.Map<TSource,TTarget>(source.Prop1);
                        //sb.AppendGenericFuncWithOneParameterCall(" = mapper.Map",
                        //    getSourcePropertyPureTypeName(p) + (p.IsNullable ? "?" : ""),
                        //    getTargetPropertyPureTypeName(p) + (p.IsNullable ? "?" : ""),
                        //    "source." + p.Name);
                        // sb.Append(";");
                    }
                    else
                    {
                        // = source.Prop1;
                        sb.Append(" = source.").Append(p.Name).Append(@";");
                    }
                }
            }

            sb.AppendLine().Append("            return target;")
                .AppendLine()
                .Append("        }");

        }

        private static void AppendEnumMapFunction(StringBuilder sb, TypeDescription type, Func<TypeDescription, string> getMapFunctionNameFunc, Func<TypeDescription, string> getSourceTypeName, Func<TypeDescription, string> getTargetTypeName, bool isExtensionMethod, bool isNullableCast)
        {
            sb.Append(@"
        public ");

            if (isExtensionMethod)
            {
                sb.Append("static ");
            }

            sb.Append(getTargetTypeName(type));

            if (isNullableCast)
            {
                sb.Append("? ");
            }
            else
            {
                sb.Append(" ");
            }

            sb.Append(getMapFunctionNameFunc(type));

            sb.Append("(");

            if (isExtensionMethod)
            {
                sb.Append("this ");
            }

            sb.Append(getSourceTypeName(type));

            if (isNullableCast)
            {
                sb.Append("? ");
            }

            sb.Append(@" source)
        {
            return (")
.Append(getTargetTypeName(type));

            if (isNullableCast)
            {
                sb.Append("?");
            }

            sb.Append(@")source;
        }
");
        }

        private static void AppendGenericFuncWithOneParameterCall(this StringBuilder sb, string funcName, string firstGenericType, string secondGenericType, string parameter)
        {
            sb.Append(funcName)
                .Append("<")
                            .Append(firstGenericType)
                            .Append(", ")
                            //.Append(p.PureType + (p.IsNullable ? "?" : ""))
                            .Append(secondGenericType)
                            .Append(">(");

            if (!string.IsNullOrWhiteSpace(parameter))
            {
                sb.Append(parameter);
            }

            sb.Append(@")");

        }

        public static void AppendCopyToArrayFunc(this StringBuilder sb)
        {
            sb.Append(@"
        
        private TTarget[] CopyArrayUsingMapper<TSource, TTarget>(TSource[] arr)
        {
            if (arr == null)
                return null;

            var newArr = new TTarget[arr.Length];

            for (int i = 0; i < arr.Length; i++)
            {
                newArr[i] = mapper.Map<TSource, TTarget>(arr[i]);
            }

            return newArr;
        }

        private TSource[] CopyArray<TSource>(TSource[] arr)
        {
            if (arr == null)
                return null;

            var newArr = new TSource[arr.Length];

            for (int i = 0; i < arr.Length; i++)
            {
                newArr[i] = arr[i];
            }

            return newArr;
        }

");

        }
    }
}
